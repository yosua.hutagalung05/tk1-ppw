# Nama Anggota :
- Hamza Daffa A
- Hasna Shafira
- Sutan Raihan M
- Yosua Hutagalung

# Link Herokuapp : 
djangu.herokuapp.com

# Cerita aplikasi dan fitur:
Selama pandemi ini disaat perlu kuliah melalui online classes, terkadang orang tua, saudara, atau orang-orang dirumah tidak tau kondisi apakah kita sedang kelas, nugas, atau mengejar deadline dan terkadang mengganggu kita saat waktu yang tidak tepat. Sehingga kita ingin membuat web yang dapat memperlihatkan status kita apakah available atau unavailable. Salah satu fiturnya juga memperlihatkan kita available atau unavailable hingga waktu tertentu. Selain itu kita juga dapat mengambah teman agar mereka bisa melihat status kita dan kita juga dapat melihat status mereka. Fitur lainnya adalah to-do-list dimana kita dapat menambahkan tugas-tugas yang perlu kita lakukan dan bila sudah selesai bisa kita tandai.

# Link Wireframe dan Prototype :
https://www.figma.com/file/OHn6p0mu1rMXev6DdQlVSX/Persona%2C-wireframe%2C-prototype?node-id=0%3A1

# Pipeline Status : 
[![pipeline status](https://gitlab.com/yosua.hutagalung05/tk1-ppw/badges/master/pipeline.svg)](https://gitlab.com/yosua.hutagalung05/tk1-ppw/-/commits/master)