from django.test import TestCase, Client
from django.contrib.auth import authenticate, get_user_model
from .views import SignUp 
from .models import Person, ToDoList
from .apps import DjanguConfig
from django.urls import resolve
from django.utils import timezone
import time

# Create your tests here.
class SigninTest(TestCase):

    def setUp(self):
        self.user = get_user_model().objects.create_user(username='test', password='12test12', email='test@example.com')
        self.user.save()

    def tearDown(self):
        self.user.delete()

    def test_correct(self):
        user = authenticate(username='test', password='12test12')
        self.assertTrue((user is not None) and user.is_authenticated)

    def test_wrong_username(self):
        user = authenticate(username='wrong', password='12test12')
        self.assertFalse(user is not None and user.is_authenticated)

    def test_wrong_pssword(self):
        user = authenticate(username='test', password='wrong')
        self.assertFalse(user is not None and user.is_authenticated)

class TestJace(TestCase):
    def test_appdjangu_name(self):
        self.assertEqual(DjanguConfig.name, "djangu")

    def test_url_is_exist_login(self):
        response = Client().get('/login/') 
        self.assertEqual(response.status_code, 200)

    def test_url_is_exist_signup(self):
        response = Client().get('/signup/') 
        self.assertEqual(response.status_code, 200)


    def test_using_template_login(self):
        response = Client().get('/login/')
        self.assertTemplateUsed(response, 'login.html')

    def test_using_template_signup(self):
        response = Client().get('/signup/')
        self.assertTemplateUsed(response, 'signup.html')

class TestYosua(TestCase):
    def test_to_do_list_template(self):
        client = Client()
        dummy_user = get_user_model().objects.create_user(username='test', password='12test12', email='test@example.com')
        dummy_user.save()
        client.login(username='test', password='12test12')

        response = client.get('/todolist/')
        self.assertTemplateUsed(response, 'todolist.html')

    def test_to_do_list_get(self):
        client = Client()
        dummy_user = get_user_model().objects.create_user(username='test', password='12test12', email='test@example.com')
        dummy_user.save()
        client.login(username='test', password='12test12')
        
        response = client.get('/todolist/')
        self.assertEqual(response.status_code, 200)
    
    def test_add_to_do_list(self):
        client = Client()
        dummy_user = get_user_model().objects.create_user(username='test', password='12test12', email='test@example.com')
        dummy_user.save()
        client.login(username='test', password='12test12')
        
        client.post('/todolist/', {'content':'Testing'})
        self.assertEqual(ToDoList.objects.all().count(), 1)
    
    def test_delete_to_do_list(self):
        client = Client()
        dummy_user = get_user_model().objects.create_user(username='test', password='12test12', email='test@example.com')
        dummy_user.save()
        client.login(username='test', password='12test12')
        
        client.post('/todolist/', {'content':'Testing'})
        to_do_obj = ToDoList.objects.get(content='Testing')
        url_str = '/deletetodo/' + str(to_do_obj.pk)
        
        client.get(url_str)
        self.assertEqual(ToDoList.objects.all().count(), 0)
    
    def test_done_to_do_list(self):
        client = Client()
        dummy_user = get_user_model().objects.create_user(username='test', password='12test12', email='test@example.com')
        dummy_user.save()
        client.login(username='test', password='12test12')

        client.post('/todolist/', {'content':'Testing'})
        to_do_obj = ToDoList.objects.get(content='Testing')
        url_str = '/donetodo/' + str(to_do_obj.pk)
        client.get(url_str)

        to_do_obj = ToDoList.objects.get(content='Testing')
        self.assertTrue(to_do_obj.completed)

    def test_undone_to_do_list(self):
        client = Client()
        dummy_user = get_user_model().objects.create_user(username='test', password='12test12', email='test@example.com')
        dummy_user.save()
        client.login(username='test', password='12test12')

        client.post('/todolist/', {'content':'Testing'})
        to_do_obj = ToDoList.objects.get(content='Testing')
        to_do_obj.completed = True
        to_do_obj.save()
        url_str = '/undonetodo/' + str(to_do_obj.pk)
        client.get(url_str)
        
        to_do_obj = ToDoList.objects.get(content='Testing')
        self.assertFalse(to_do_obj.completed)

class TestHasna(TestCase):
    def test_url_status_login(self):
        client = Client()
        dummy_user = get_user_model().objects.create_user(username='test', password='12test12', email='test@example.com')
        dummy_user.save()
        client.login(username='test', password='12test12')

        response = client.get('/isistatus/') 
        self.assertEqual(response.status_code, 200)

    def test_template_status(self):
        client = Client()
        dummy_user = get_user_model().objects.create_user(username='test', password='12test12', email='test@example.com')
        dummy_user.save()
        client.login(username='test', password='12test12')

        response = client.get('/isistatus/')
        self.assertTemplateUsed(response, 'isistatus.html')

    def test_apakah_ada_status(self):
        client = Client()
        dummy_user = get_user_model().objects.create_user(username='test', password='12test12', email='test@example.com')
        dummy_user.save()
        client.login(username='test', password='12test12')

        response = client.get('/isistatus/') 
        html_kembalian = response.content.decode('utf8')
        self.assertIn('Status', html_kembalian)

    def test_url_test_login(self):
        client = Client()
        dummy_user = get_user_model().objects.create_user(username='test', password='12test12', email='test@example.com')
        dummy_user.save()
        client.login(username='test', password='12test12')

        response = client.get('/test/') 
        self.assertEqual(response.status_code, 200)

    def test_template_status(self):
        client = Client()
        dummy_user = get_user_model().objects.create_user(username='test', password='12test12', email='test@example.com')
        dummy_user.save()
        client.login(username='test', password='12test12')

        response = client.get('/test/')
        self.assertTemplateUsed(response, 'test.html')
