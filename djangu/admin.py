from django.contrib import admin
from .models import Person, Friend_Request, Status, ToDoList

# Register your models here.
admin.site.register(Person)
admin.site.register(Friend_Request)
admin.site.register(Status)
admin.site.register(ToDoList)