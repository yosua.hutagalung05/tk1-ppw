from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserCreationForm
from django import forms
from .models import Status, ToDoList
from django.forms import ModelForm


class UserCreateForm(UserCreationForm):
    
    class Meta:
        fields = ('username','email','password1','password2')
        model = get_user_model()

    def __init__(self,*args,**kwargs):
        super().__init__(*args,**kwargs)
        self.fields['username'].label = 'Username'
        self.fields['email'].label = "Email Adress"

class StatusForm(forms.ModelForm):
    class Meta:
        model = Status
        fields = [
            'state',
            'date',
            'context'
            ]
        widgets = {
            'state' : forms.RadioSelect(attrs={'Available':'Available', 'Unavailable': 'Unavailable'}),
            'date': forms.TextInput(attrs={'placeholder':'Year-Month-Date 00:00:00', 'class': "formstatus"}),
            'context': forms.TextInput(attrs={'placeholder':'Context', 'class': "formstatus"})
        }

class ToDoListForm(forms.ModelForm):
    class Meta:
        model = ToDoList
        fields = {
            'content',
        }
        widgets = {
             'content' : forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'What do you want to do?'}),
        }
    
