from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.timezone import now 

# Create your models here.
# class User(auth.models.User,auth.models.PermissionsMixin):
#     def __str__(self):
#         return "@{}".format(self.username)

class Person(models.Model):
    userlink = models.OneToOneField(User, on_delete=models.CASCADE)
    friends = models.ManyToManyField("Person", blank=True)
   
    def __str__(self):
        return str(self.userlink.username)

class Status(models.Model):
    context =  models.CharField(max_length=100, blank=False, default=None)
    date = models.DateTimeField(default=None, null=False, blank=False)
    namastatus = models.ForeignKey(Person, on_delete=models.CASCADE)
    types = (('Available', "Available"),
            ('Unavailable', "Unavailable"))
    state = models.CharField(max_length=100, choices=types, default='Available')

class Friend_Request(models.Model):
    from_user = models.ForeignKey(Person, related_name="from_user", on_delete=models.CASCADE, null=True)
    to_user = models.ForeignKey(Person, related_name="to_user", on_delete=models.CASCADE, null=True)

class ToDoList(models.Model):
    user = models.ForeignKey(Person, on_delete=models.CASCADE)
    content = models.CharField(max_length=100, blank=True)
    completed = models.BooleanField(default=False)
    
@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Person.objects.create(userlink=instance)

# @receiver(post_save, sender=User)
# def save_user_profile(sender, instance, **kwargs):
#     instance.profile.save()
