from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import CreateView
from django.views.generic import TemplateView
from django.shortcuts import render, redirect, get_object_or_404
from . import forms 
from django.http import HttpResponse
from .models import Friend_Request, Person, Status, ToDoList
from .forms import StatusForm
from django.contrib.auth.decorators import login_required
from django.contrib.auth import get_user_model
from django.db.models import Q
from django.utils import timezone
from datetime import datetime

# Create your views here.
User = get_user_model()

def index(request):
    return render(request, 'index.html')

class SignUp(CreateView):
    form_class = forms.UserCreateForm
    success_url = reverse_lazy('djangu:login')
    template_name = 'signup.html'

class TestPage(TemplateView):
    template_name = "test.html"

def SearchFriends(request):
    return render(request,'friends.html')
    
def acceptfriends(request):
    this_user = Person.objects.get(userlink=request.user)
    you = this_user.userlink
    rec_friends = Friend_Request.objects.filter(to_user=this_user)
    friend = this_user.friends.all()
    return render(request, 'acceptfriends.html', {'rec_friends':rec_friends,'friend':friend})

def friend_list(request):
    this_person = Person.objects.get(userlink=request.user)
    friend = this_person.friends.all()
    try:
        ada_status = Status.objects.get(namastatus=this_person)
        now = timezone.now()
        if (now >= ada_status.date):
            Status.objects.get(namastatus=this_person).delete()
            ada_status = None
        status_teman = Status.objects.filter(namastatus__in=friend, date__gte=now)
        return render(request, 'test.html', {'friend' : friend, 'ada_status':ada_status, 'status_teman':status_teman, 'saya':this_person})
    except Status.DoesNotExist:
        status_teman = Status.objects.filter(namastatus__in=friend)
        return render(request, 'test.html', {'friend' : friend, 'status_teman':status_teman,'saya':this_person})
         
def delete_friend(request, requestID):
    this_user = Person.objects.get(userlink=request.user)
    friend_profile = get_object_or_404(Person, id=requestID)
    this_user.friends.remove(friend_profile)
    friend_profile.friends.remove(this_user)
    return redirect('/acceptfriends')

# TINGGAL MODIF STATUS FORM DARI SINI YA
def status(request): 
    upload = StatusForm()
    this_user = Person.objects.get(userlink=request.user)
    if request.method == 'POST':
        upload = StatusForm(request.POST or None)
        if upload.is_valid():
            try:
                obj = Status.objects.get(namastatus=this_user)
            except Status.DoesNotExist:
                obj = None
            finally:
                if obj != None:
                    obj.delete()
                    update_obj=Status(state=upload.data['state'], context=upload.data['context'], date=upload.data['date'], namastatus=this_user)
                    update_obj.save()
                    return redirect('/test')
                else :
                    obj=Status(state=upload.data['state'], context=upload.data['context'], date=upload.data['date'], namastatus=this_user)
                    obj.save()
                    return redirect('/test')
        return render(request, 'isistatus.html', {'status' : upload, 'saya':this_user})
    else :
        return render(request, 'isistatus.html', {'status' : upload, 'saya':this_user})
        
@login_required
def send_friend_request(request, userfromID, userID):
    from_user = Person.objects.get(id=userfromID)
    to_user = Person.objects.get(id=userID)
    friend_request, created = Friend_Request.objects.get_or_create(from_user=from_user, to_user=to_user)
    return render(request,"friends.html")

@login_required
def accept_friend_request(request, requestID):
    friend_request = Friend_Request.objects.get(id=requestID)
    if friend_request.to_user.userlink == request.user:
        friend_request.to_user.friends.add(friend_request.from_user)
        friend_request.from_user.friends.add(friend_request.to_user)
        friend_request.delete()
        return HttpResponse('friends request accepted')
    else:
        return HttpResponse('Error')

@login_required
def ketolak(request, requestID):
    friend_request = Friend_Request.objects.get(id=requestID)
    if friend_request.to_user.userlink == request.user:
        friend_request.delete()
        return HttpResponse('friends request declined')
    else:
        return HttpResponse('Error')

@login_required
def findfriends(request):
    from_user = Person.objects.get(userlink=request.user)
    query = request.GET.get('q')
    cari = User.objects.filter(Q(username__icontains = query))
    return render(request, 'friends.html', {'cari':cari,'from_user':from_user})

@login_required
def to_do_list(request):
    current_user = Person.objects.get(userlink=request.user)
    form = forms.ToDoListForm(request.POST or None)
    to_do_list = ToDoList.objects.all()
    if request.method == 'GET':
        response = {
            'form': form,
            'to_do_list': to_do_list,
            'current_user': current_user,
        }
        return render(request, 'todolist.html', response)
    elif form.is_valid() and request.method == 'POST':
        to_do = ToDoList(user=current_user, content=form.data['content'])
        to_do.save()
        return redirect('/todolist/')

@login_required
def delete_to_do_list(request, pk):
    to_do = ToDoList.objects.get(pk=pk)
    to_do.delete()
    return redirect('/todolist/')

@login_required
def done_to_do_list(request, pk):
    to_do = ToDoList.objects.get(pk=pk)
    to_do.completed = True
    to_do.save()
    return redirect('/todolist/')

@login_required
def undone_to_do_list(request, pk):
    to_do = ToDoList.objects.get(pk=pk)
    to_do.completed = False
    to_do.save()
    return redirect('/todolist/')